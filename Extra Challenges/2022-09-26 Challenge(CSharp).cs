//The question we'll work through is the following: return a new sorted merged list from K sorted lists, each with size N.
//Before we move on any further, you should take some time to think about the solution!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2022_09_26_Challenge_CSharp_
{
    public class Program
    {
        static void Main(string[] args)
        {
            //--> Setting Up variables for the Challenge

            int K = 3; //--> Number of lists
            int N = 3; //--> Size of lists

            Random random = new Random();
            List<List<int>> MainList = new List<List<int>>();
            List<int> sortedList = new List<int>();

            for (var i= 0; i < K; i++)
            {
                List<int> list = new List<int>();
                for (var j= 0; j < N; j++)
                {
                    list.Add(random.Next(1,100));
                }
                list.Sort();
                MainList.Add(list);
            }
            //--> Setting Up End

            //--> First Solution:
            //// Using Built-In method "Sort" for Lists.
            void MergeSortedLists1(List<List<int>> lists)
            {
                foreach (var list in lists)
                {
                    foreach (var number in list)
                    {
                        sortedList.Add(number); //--> Adds every number in the lists into sortedList variable
                    }
                }
                
                sortedList.Sort(); //--> Sorts the list
            }

            //--> Second Solution:
            //// Sorting the items manually, checking the list from the start and inserting while sorting at the same time.
            void MergeSortedLists2(List<List<int>> lists) //--> Sorted Manually
            {
                foreach (var list in lists)
                {
                    foreach (var number in list)
                    {
                        if (sortedList == null) sortedList.Add(number);
                        else
                        {
                            bool inserted = false;
                            for (int i = 0; i < sortedList.Count(); i++)
                            {
                                if (number < sortedList[i]) 
                                {
                                    sortedList.Insert(i, number);
                                    inserted = true;
                                    break;
                                }
                            }
                            if (!inserted) sortedList.Add(number);
                        }
                    }
                }

            }

            //First Solution
            //MergeSortedLists1(MainList); //--> Function call

            //Second Solution
            MergeSortedLists2(MainList); //--> Function call

            //--> Write sorted list
            foreach (var item in sortedList)
            {
                Console.WriteLine(item);
            }
        }
    }
}

//First solution time (including stting up): 15 mins
//Second solution time: 30 mins.
