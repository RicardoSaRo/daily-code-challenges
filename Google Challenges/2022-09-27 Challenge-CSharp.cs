//Good morning! Here's your coding interview problem for today.
//This problem was asked by Google.
//Given the root to a binary tree, implement serialize(root), which serializes the tree into a string, and deserialize(s),
//which deserializes the string back into the tree.
//For example, given the following Node class
//
//class Node :
//    def __init__(self, val, left=None, right=None):
//        self.val = val
//        self.left = left
//        self.right = right
//
//The following test should pass:
//
//node = Node('root', Node('left', Node('left.left')), Node('right'))
//assert deserialize(serialize(node)).left.left.val == 'left.left'

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27_09_2022_Challenge
{
    public class Node
    {
        public Node(string val, Node left, Node right)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public string val { get; set; }
        public Node left { get; set; }
        public Node right { get; set; }
    }

    public class Program
    {
        static string serial { get; set; } = ""; //--> Global Variable to store serialization

        static void Main(string[] args)
        {
            //--> Recursive functions that checks everynode to serialize into a string
            string Serialize(Node snode)
            {
                string nullcheckL = snode.left == null ? "null" : snode.left.val;
                string nullcheckR = snode.right == null ? "null" : snode.right.val;
                string nodeString = "*Node.Val:" + snode.val + ",Node.Left:" + nullcheckL + ",Node.Right:" + nullcheckR;
                Program.serial += nodeString;
                if (snode.left != null) Serialize(snode.left);
                if (snode.right != null) Serialize(snode.right);
                return Program.serial;
            }

            //--> Recursive function that deserialize the string back into a tree
            Node Deserialize(string snode)
            {
                string nodecut = snode.Substring(0, snode.IndexOf("*",1)); //--> Cuts the first node in the string
                snode = snode.Replace(nodecut, ""); //--> Deletes the node in the main string to be used in recursion
                string[] preNode = nodecut.Split(',');//--> Splits the node parts into an array to compare better
                
                //--> initialize variables to create new node and assign values
                string nodeVal = null;
                Node nodeLeft = null;
                Node nodeRight = null;
                Node nNode = new Node(nodeVal,nodeLeft,nodeRight); //--> Creates Node to assign info from the cut

                //--> Goes through the array checking values from the array and assign. If a node pointer is not null
                //--> Calls itself again to create a new node for that pointer in recursion.
                foreach (var item in preNode)
                {
                    if (item.Contains("*Node.Val:"))
                    {
                        nodeVal = item.Replace("*Node.Val:","");
                        nNode.val = nodeVal;
                    }
                    else if (item.Contains("Node.Left:"))
                    {
                        if(item.Replace("Node.Left:", "") != "null")
                            nNode.left = Deserialize(snode);
                    }
                    else if(item.Contains("Node.Right:"))
                    {
                        if(item.Replace("Node.Right:", "") != "null")
                            nNode.right = Deserialize(snode);
                    }
                }
                return nNode;
            }

            //--> Set Up
            Node node = new Node("root",new Node("left",new Node("left.left",null,null),null), new Node("right",null,null));

            //--> Calls funtions
            node = Deserialize(Serialize(node));

            //--> Checks if after serialization and deserialization the values asked in the challenge are true (they are!).
            Console.WriteLine(node.left.left.val == "left.left");
        }
    }
}

//--> Challenge Finished. The reasoning for the problem took me 25 about mins. I could not implement it quickly because I had
//--> to search and test some stuff to properly string Indexing, Replacing, Splitting, etc.
