﻿//Good morning! Here's your coding interview problem for today.
//This problem was recently asked by Google.
//Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
//For example, given[10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
//Bonus: Can you do this in one pass?

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2022_09_25_Challenge_CSharp
{
    internal class FindIf2NumberInArraySumsK
    {
        static void Main(string[] args)
        {
            int[] mainlist = { 1, 15, 10, 7 };
            
            int maink = 17;
            int sumcheck = 0;

            bool foundSum(int[] list, int k)
            {
                int i = 1;
                while (list.Length > 1)
                {
                    if (list.Length <= 1) return false;
                    sumcheck = list[0] + list[i];
                    if (sumcheck == k) return true;
                    i++;
                    if (i >= list.Length)
                    {
                        list = list.Reverse().Take(list.Length-1).Reverse().ToArray();
                        i = 1;
                    }
                }
                return false;
            }

            bool sumMatch = foundSum(mainlist, maink);
            Console.WriteLine(sumMatch);
        }
    }
}

// The function "foundSum" takes the array and compares the first number with the others. If at any point sumcheck is equals to k,
// the function returns true. If the sum of the first number compared with the others is not equals to k, the fist number is deleted
// from the array to reduce the lenght and check the other numbers. Because its done in a While loop, it will keep repeating until
// the sum of 2 numbers equals k (true) or until there is only 1 number in the array (false).
// For this reason the Challenge is complete and meeting the Bonus condition!
