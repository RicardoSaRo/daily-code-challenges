//Good morning! Here's your coding interview problem for today.
//This problem was asked by Uber.
//Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in
//the original array except the one at i.
//For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1],
//the expected output would be [2, 3, 6].
//Follow-up: what if you can't use division?

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2022_09_26_Challenge_CSharp
{
    public class ProductArrayButIndex
    {
        static void Main(string[] args)
        {
            int[] mainlist = { 1,2,3,4,5 };
            int[] resultsArray; 

            int[] MultiplyArrayButIndex(int[] list)
            {
                int total = 0; //--> To store the products
                List<int> resultList = new List<int>(); //--> To store results (easier to Add and at the end converts into array)

                for (var i = 0; i < list.Length; i++)
                {
                    total = 1; //--> clears variable at each start
                    
                    for (var j = 0; j < list.Length; j++)
                    {
                        if (i != j) total *= list[j]; //--> Multiplies every item on array but the item index
                    }

                    resultList.Add(total); //--> adds it to the end of a list
                }

                return resultList.ToArray(); //--> Converts list into an array
            }

            resultsArray = MultiplyArrayButIndex(mainlist); //--> Function call

            Array.ForEach(resultsArray, Console.WriteLine);
        }
    }
}

// The first FOR loop helps to check the Indexes with the second FOR loop and prevents each value to multiply by itself.
// For this reason the Challenge is complete without even using division!
