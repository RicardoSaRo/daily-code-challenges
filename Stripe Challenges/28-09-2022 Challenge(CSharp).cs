﻿//Good morning! Here's your coding interview problem for today.
//This problem was asked by Stripe.
//Given an array of integers, find the first missing positive integer in linear time and constant space.
//In other words, find the lowest positive integer that does not exist in the array.
//The array can contain duplicates and negative numbers as well.
//For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.
//You can modify the input array in-place.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _28_09_2022_Challenge
{
    public class Program
    {
        static void Main(string[] args)
        {
            int[] mainArray = { 3, 4, -1, 1 };
            bool ocurrence = true;
            int temp = 1; //--> Lowest intenger possible

            while(ocurrence)
            {
                ocurrence = false;
                foreach (var item in mainArray)
                {
                    if (item == temp && item >= 1)
                    {
                        temp++;
                        ocurrence = true;
                    }
                }
            }

            Console.WriteLine(temp);
        }
    }
}

//Challenge finished in around 15 mins. Not much too add.
